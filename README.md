# DesQ Volume
Volume is the DesQ equivalent of pavucontrol-qt.


### Dependencies:
* Qt6
* libcanberra
* [libdesq-qt6](https://gitlab.com/DesQ/libdesq)
* [libdesqui-qt6](https://gitlab.com/DesQ/libdesqui)
* [DFL::Applications](https://gitlab.com/desktop-frameworks/applications)
* [DFL::Settings](https://gitlab.com/desktop-frameworks/settings)
* [DFL::Volume](https://gitlab.com/desktop-frameworks/volume)
* [DFL::Utils](https://gitlab.com/desktop-frameworks/utils)
* [DFL::XDG](https://gitlab.com/desktop-frameworks/xdg)


### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQUtils/Volume.git DesQVolume`
- Enter the `DesQVolume` folder
  * `cd DesQVolume`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Please test and let me know


### Upcoming
* Volume Controls from the tray
* Adjusting Stream volumes from the tray
