/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Plugin.hpp"
#include "VolumeWidget.hpp"

/* Name of the plugin */
QString VolumePlugin::name() {
    return "DesQ Volume";
}


/* Name of the plugin */
QIcon VolumePlugin::icon() {
    return QIcon::fromTheme( "desq" );
}


/* The plugin version */
QString VolumePlugin::version() {
    return QString( "1.0.0" );
}


/* The SNI plugin */
DesQ::Shell::PluginWidget *VolumePlugin::widget( QWidget *parent ) {
    return new VolumeWidget( parent );
}
