/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <canberra.h>

#include <QtWidgets>
#include "VolumeWidget.hpp"

VolumeWidget::VolumeWidget( QWidget *parent ) : DesQ::Shell::PluginWidget( parent ) {
    /* Init defaultSink, lastUsed's to 0 */
    defaultSink   = nullptr;
    defaultSource = nullptr;

    display = new QToolButton( this );
    display->setIconSize( QSize( 64, 64 ) );
    display->setIcon( QIcon( ":/icons/speaker.png" ) );
    display->setFixedSize( 72, 72 );
    display->setAutoRaise( true );
    display->setFocusPolicy( Qt::NoFocus );

    setFixedSize( 200, 120 );

    sinkDevIcon = new QLabel();
    sinkDevIcon->setFixedSize( 16, 16 );
    sinkDevIcon->setPixmap( QPixmap( ":/icons/audio-speaker.png" ) );

    sinkDevName = new QLabel();
    sinkDevName->setFixedHeight( 16 );
    sinkDevName->setText( "Sink" );

    sinkVolBar = new QSlider( Qt::Horizontal );
    sinkVolBar->setRange( 0, 150 );
    sinkVolBar->setTickInterval( 100 );
    sinkVolBar->setTickPosition( QSlider::TicksBelow );
    sinkVolBar->setSingleStep( 1 );
    sinkVolBar->setPageStep( 10 );
    sinkVolBar->setValue( 50 );
    sinkVolBar->setFocusPolicy( Qt::NoFocus );

    sourceDevIcon = new QLabel();
    sourceDevIcon->setFixedSize( 16, 16 );
    sourceDevIcon->setPixmap( QIcon::fromTheme( "audio-input-microphone" ).pixmap( 16 ) );

    sourceDevName = new QLabel();
    sourceDevName->setFixedHeight( 16 );
    sourceDevName->setText( "Source" );

    sourceVolBar = new QSlider( Qt::Horizontal );
    sourceVolBar->setRange( 0, 150 );
    sourceVolBar->setTickInterval( 100 );
    sourceVolBar->setTickPosition( QSlider::TicksBelow );
    sourceVolBar->setSingleStep( 1 );
    sourceVolBar->setPageStep( 10 );
    sourceVolBar->setValue( 50 );
    sourceVolBar->setFocusPolicy( Qt::NoFocus );

    connect( display,      &QToolButton::clicked, this, &VolumeWidget::manageSinkMute );
    connect( display,      &QToolButton::clicked, this, &VolumeWidget::manageSourceMute );

    connect( sinkVolBar,   &QSlider::sliderMoved, this, &VolumeWidget::setSinkVolume );
    connect( sourceVolBar, &QSlider::sliderMoved, this, &VolumeWidget::setSourceVolume );

    QGridLayout *lyt = new QGridLayout();

    lyt->addWidget( display,       0, 0, 5, 1, Qt::AlignCenter );

    lyt->addWidget( sinkDevIcon,   0, 1, Qt::AlignCenter );
    lyt->addWidget( sinkDevName,   0, 2, Qt::AlignCenter );
    lyt->addWidget( sinkVolBar,    1, 1, 1, 2, Qt::AlignCenter );

    lyt->addWidget( sourceDevIcon, 3, 1, Qt::AlignCenter );
    lyt->addWidget( sourceDevName, 3, 2, Qt::AlignCenter );
    lyt->addWidget( sourceVolBar,  4, 1, 1, 2, Qt::AlignCenter );

    setLayout( lyt );

    opacity = new QGraphicsOpacityEffect( this );
    opacity->setOpacity( 0.4 );
    setGraphicsEffect( opacity );

    initPulseAudio();
}


void VolumeWidget::initPulseAudio() {
    PulseAudioQt::Context *paCtx = PulseAudioQt::Context::instance();
    PulseAudioQt::Server  *paSrv = paCtx->server();

    connect(
        paSrv, &PulseAudioQt::Server::defaultSinkChanged, [ = ]( PulseAudioQt::Sink *sink ) {
            if ( defaultSink ) {
                /* Disconnect the older sink */
                defaultSink->disconnect();
            }

            /* Reset the sink */
            defaultSink = sink;

            /** defaultSink can be nullptr; beware */
            if ( not defaultSink ) {
                return;
            }

            /* Reset the volume */
            sinkVolBar->setValue( ( int )( 100.0 * sink->volume() / PulseAudioQt::normalVolume() ) );

            /* Default port: handleSinkPorts() performs an init :P */
            handleSinkPorts();

            /* Initialzie the mute state */
            manageSinkMute();

            connect(
                defaultSink, &PulseAudioQt::Sink::volumeChanged, [ = ]() {
                    /* Change the bar position only if the change was external to this UI */
                    if ( not sinkVolBar->isSliderDown() ) {
                        if ( 100.0 * sink->volume() / PulseAudioQt::normalVolume() > 150 ) {
                            sinkVolBar->setValue( 150 );
                        }

                        else {
                            sinkVolBar->setValue( ( int )( 100.0 * sink->volume() / PulseAudioQt::normalVolume() ) );
                        }
                    }
                }
            );

            connect( defaultSink, &PulseAudioQt::Sink::mutedChanged, this, &VolumeWidget::manageSinkMute );
            connect( defaultSink, &PulseAudioQt::Sink::portsChanged, this, &VolumeWidget::handleSinkPorts );
        }
    );

    connect(
        paSrv, &PulseAudioQt::Server::defaultSourceChanged, [ = ]( PulseAudioQt::Source *source ) {
            if ( defaultSource ) {
                /* Disconnect the older source */
                defaultSource->disconnect();
            }

            /* Reset the source */
            defaultSource = source;

            /** defaultSource can be nullptr; beware */
            if ( not defaultSource ) {
                return;
            }

            /* Reset the volume */
            sourceVolBar->setValue( ( int )( 100.0 * source->volume() / PulseAudioQt::normalVolume() ) );

            /* Default port: handlesourcePorts() performs an init :P */
            handleSourcePorts();

            /* Initialzie the mute state */
            manageSourceMute();

            connect(
                defaultSource, &PulseAudioQt::Source::volumeChanged, [ = ]() {
                    /* Change the bar position only if the change was external to this UI */
                    if ( not sinkVolBar->isSliderDown() ) {
                        if ( 100.0 * source->volume() / PulseAudioQt::normalVolume() > 150 ) {
                            sourceVolBar->setValue( 150 );
                        }

                        else {
                            sourceVolBar->setValue( ( int )( 100.0 * source->volume() / PulseAudioQt::normalVolume() ) );
                        }
                    }
                }
            );

            connect( defaultSource, &PulseAudioQt::Source::mutedChanged, this, &VolumeWidget::manageSourceMute );
            connect( defaultSource, &PulseAudioQt::Source::portsChanged, this, &VolumeWidget::handleSourcePorts );
        }
    );
}


void VolumeWidget::manageSinkMute() {
    if ( not defaultSink ) {
        return;
    }

    /* Disconnect to avoid double chirps/loops */
    disconnect( defaultSink, &PulseAudioQt::Sink::mutedChanged, this, &VolumeWidget::manageSourceMute );

    /* Mute the device only if we clicked the button */
    if ( qobject_cast<QToolButton *>( sender() ) ) {
        isSinkMuted = not defaultSink->isMuted();
        sinkVolBar->setEnabled( not isSinkMuted );
        defaultSink->setMuted( isSinkMuted );
    }

    /* We came from external mute toggle. */
    else {
        isSinkMuted = defaultSink->isMuted();
        sinkVolBar->setEnabled( not isSinkMuted );
    }

    display->setIcon( isSinkMuted ? QIcon( ":/icons/speaker-muted.png" ) : QIcon( ":/icons/speaker.png" ) );

    /* Reconnect */
    connect( defaultSink, &PulseAudioQt::Sink::mutedChanged, this, &VolumeWidget::manageSourceMute );
}


void VolumeWidget::handleSinkPorts() {
    /////
    // TODO
    // We should include code to mute/pause audio when the headphones/headset are removed
    // We should also include code to mute/pause/reduce-volume if the volume appears too high
    /////

    /////
    // Ports choice algo
    // 1. Check if headset/headphones are available. If yes, set that as the active port.
    // 2. Otherwise, get the port with highest priority which is not Unavailable. Set that as active port.
    // 3. If we just removed the headphones/headset, pause all streams/or mute audio. (User setting)
    /////

    if ( not defaultSink ) {
        return;
    }

    /* Disconnect all port signals */
    sinkPorts.clear();

    /* Re-init the list */
    sinkPorts = defaultSink->ports().toList();
    for ( PulseAudioQt::Port *port: sinkPorts ) {
        connect( port, &PulseAudioQt::Port::availabilityChanged, this, &VolumeWidget::handleSinkPorts );
    }

    /* Start searching for the default port to use */
    PulseAudioQt::Port *port    = nullptr;
    quint32            priority = 0;

    /* Headphone/headset Search: Choose the highest priority headphones */
    for ( PulseAudioQt::Port *p: sinkPorts ) {
        if ( p->name().toLower().contains( "headphone" ) or p->description().toLower().contains( "headphone" ) ) {
            if ( p->availability() == PulseAudioQt::Port::Unavailable ) {
                continue;
            }

            if ( p->priority() > priority ) {
                priority = p->priority();
                port     = p;
                break;
            }
        }

        else if ( p->name().toLower().contains( "headset" ) or p->description().toLower().contains( "headset" ) ) {
            if ( p->availability() == PulseAudioQt::Port::Unavailable ) {
                continue;
            }

            if ( p->priority() > priority ) {
                priority = p->priority();
                port     = p;
                break;
            }
        }
    }

    /* If it's a headphone/headset */
    if ( port ) {
        /* Set the active port */
        defaultSink->setActivePortIndex( defaultSink->ports().indexOf( port ) );

        /* Set suitable icon */
        sinkDevIcon->setPixmap( QPixmap( ":/icons/audio-headphones.png" ) );

        /* Remember the last used port */
        if ( port->description().toLower().contains( "headphone" ) ) {
            lastUsedSinkPort = "headphone";
        }

        else if ( port->description().toLower().contains( "headset" ) ) {
            lastUsedSinkPort = "headphone";
        }

        else {
            lastUsedSinkPort = "speakers";
        }

        /* Get the volume of the port */
        int volPC = ( int )( 100.0 * defaultSink->volume() / PulseAudioQt::normalVolume() );

        /* Set the high volume alert */
        if ( volPC >= 75 ) {
            qDebug() << "High volume alert!!";
        }

        /* Set the volume on the @sinkVolBar */
        sinkVolBar->setValue( volPC );

        return;
    }

    /* Currently headphones are not connected. Check if @lastUsedSinkPort was headphones or headset */
    if ( lastUsedSinkPort == "headphone" ) {
        // if ( theUserWantsIt )
        QProcess::startDetached( "playerctl", { "pause" } );

        // if ( aggressive )
        // defaultSink->setMuted( true )
    }

    port     = nullptr;
    priority = 0;

    /* Choose the highest device which is not unavailable */
    for ( PulseAudioQt::Port *p: defaultSink->ports() ) {
        if ( p->availability() == PulseAudioQt::Port::Unavailable ) {
            continue;
        }

        if ( p->priority() > priority ) {
            priority = p->priority();
            port     = p;
            break;
        }
    }

    /* If we have a valid port */
    if ( port ) {
        /* Set the active port */
        defaultSink->setActivePortIndex( defaultSink->ports().indexOf( port ) );

        /* Set suitable icon */
        sinkDevIcon->setPixmap( QPixmap( ":/icons/audio-speaker.png" ) );

        /* Remember the last used port */
        if ( port->description().toLower().contains( "headphone" ) ) {
            lastUsedSinkPort = "headphone";
        }

        else if ( port->description().toLower().contains( "headset" ) ) {
            lastUsedSinkPort = "headphone";
        }

        else {
            lastUsedSinkPort = "speakers";
        }

        /* Get the volume of the port */
        int volPC = ( int )( 100.0 * defaultSink->volume() / PulseAudioQt::normalVolume() );

        /* Set the high volume alert */
        if ( volPC >= 75 ) {
            qDebug() << "High volume alert!!";
        }

        /* Set the volume on the @sinkVolBar */
        sinkVolBar->setValue( volPC );

        return;
    }
}


void VolumeWidget::setSinkVolume( int vol ) {
    int aVol = ( int )( vol * PulseAudioQt::normalVolume() / 100 );

    if ( defaultSink ) {
        defaultSink->setVolume( aVol );
        qDebug() << "Volume set to:" << aVol << vol;
    }
}


void VolumeWidget::manageSourceMute() {
    if ( not defaultSource ) {
        return;
    }

    /* Disconnect to avoid double chirps/loops */
    disconnect( defaultSource, &PulseAudioQt::Sink::mutedChanged, this, &VolumeWidget::manageSourceMute );

    /* Mute the device only if we clicked the button */
    if ( qobject_cast<QToolButton *>( sender() ) ) {
        isSourceMuted = not defaultSource->isMuted();
        sourceVolBar->setEnabled( not isSourceMuted );
        defaultSource->setMuted( isSourceMuted );
    }

    /* We came from external mute toggle. */
    else {
        isSourceMuted = defaultSource->isMuted();
        sourceVolBar->setEnabled( not isSourceMuted );
    }

    /* Reconnect */
    connect( defaultSource, &PulseAudioQt::Sink::mutedChanged, this, &VolumeWidget::manageSourceMute );
}


void VolumeWidget::handleSourcePorts() {
    /////
    // TODO
    // We should include code to mute/pause audio when the headphones/headset are removed
    // We should also include code to mute/pause/reduce-volume if the volume appears too high
    /////

    /////
    // Ports choice algo
    // 1. Check if headset/headphones are available. If yes, set that as the active port.
    // 2. Otherwise, get the port with highest priority which is not Unavailable. Set that as active port.
    // 3. If we just removed the headphones/headset, pause all streams/or mute audio. (User setting)
    /////

    if ( not defaultSource ) {
        return;
    }

    /* Disconnect all port signals */
    sourcePorts.clear();

    /* Re-init the list */
    sourcePorts = defaultSource->ports().toList();
    for ( PulseAudioQt::Port *port: sourcePorts ) {
        connect( port, &PulseAudioQt::Port::availabilityChanged, this, &VolumeWidget::handleSourcePorts );
    }

    /* Start searching for the default port to use */
    PulseAudioQt::Port *port    = nullptr;
    quint32            priority = 0;

    /* Headphone/headset Search: Choose the highest priority headphones */
    for ( PulseAudioQt::Port *p: sourcePorts ) {
        if ( p->name().toLower().contains( "headphone" ) or p->description().toLower().contains( "headphone" ) ) {
            if ( p->availability() == PulseAudioQt::Port::Unavailable ) {
                continue;
            }

            if ( p->priority() > priority ) {
                priority = p->priority();
                port     = p;
                break;
            }
        }

        else if ( p->name().toLower().contains( "headset" ) or p->description().toLower().contains( "headset" ) ) {
            if ( p->availability() == PulseAudioQt::Port::Unavailable ) {
                continue;
            }

            if ( p->priority() > priority ) {
                priority = p->priority();
                port     = p;
                break;
            }
        }
    }

    /* If it's a headphone/headset */
    if ( port ) {
        /* Set the active port */
        defaultSource->setActivePortIndex( defaultSource->ports().indexOf( port ) );

        /* Set suitable icon */
        sourceDevIcon->setPixmap( QPixmap( ":/icons/audio-headphones.png" ) );

        /* Remember the last used port */
        if ( port->description().toLower().contains( "headphone" ) ) {
            lastUsedSourcePort = "headphone";
        }

        else if ( port->description().toLower().contains( "headset" ) ) {
            lastUsedSourcePort = "headphone";
        }

        else {
            lastUsedSourcePort = "speakers";
        }

        /* Get the volume of the port */
        int volPC = ( int )( 100.0 * defaultSource->volume() / PulseAudioQt::normalVolume() );

        /* Set the high volume alert */
        if ( volPC >= 75 ) {
            qDebug() << "High volume alert!!";
        }

        /* Set the volume on the @sinkVolBar */
        sourceVolBar->setValue( volPC );

        return;
    }
}


void VolumeWidget::setSourceVolume( int vol ) {
    int aVol = ( int )( vol * PulseAudioQt::normalVolume() / 100 );

    if ( defaultSource ) {
        defaultSource->setVolume( aVol );
        qDebug() << "Volume set to:" << aVol << vol;
    }
}


void VolumeWidget::enterEvent( QMouseEnterEvent *event ) {
    opacity->setOpacity( 1.0 );
    event->accept();
}


void VolumeWidget::leaveEvent( QEvent *event ) {
    opacity->setOpacity( 0.4 );
    event->accept();
}
