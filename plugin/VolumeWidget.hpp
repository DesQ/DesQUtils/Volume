/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QWidget>
#include <QLabel>
#include <QSlider>
#include <QToolButton>
#include <QGraphicsOpacityEffect>
#include <desqui/ShellPlugin.hpp>

#if QT_VERSION < QT_VERSION_CHECK( 6, 0, 0 )
    #define QMouseEnterEvent    QEvent
#else
    #define QMouseEnterEvent    QEnterEvent
#endif

class VolumeWidget : public DesQ::Shell::PluginWidget {
    Q_OBJECT

    public:
        VolumeWidget( QWidget *parent = 0 );

    private:
        QByteArray svg;
        QToolButton *display;

        QLabel *sinkDevIcon;
        QLabel *sinkDevName;
        QSlider *sinkVolBar;

        QLabel *sourceDevIcon;
        QLabel *sourceDevName;
        QSlider *sourceVolBar;

        QGraphicsOpacityEffect *opacity;

        /* Audio variables */
        PulseAudioQt::Sink *defaultSink;
        QList<PulseAudioQt::Port *> sinkPorts;
        QString lastUsedSinkPort;
        bool isSinkMuted = false;

        /* Audio variables */
        PulseAudioQt::Source *defaultSource;
        QList<PulseAudioQt::Port *> sourcePorts;
        QString lastUsedSourcePort;
        bool isSourceMuted = false;

        void initPulseAudio();

        void manageSinkMute();
        void handleSinkPorts();
        void setSinkVolume( int vol );

        void manageSourceMute();
        void handleSourcePorts();
        void setSourceVolume( int vol );

    protected:
        void enterEvent( QMouseEnterEvent * );
        void leaveEvent( QEvent * );
};
