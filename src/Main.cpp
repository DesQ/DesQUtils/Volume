/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QCoreApplication>
#include <QDebug>
#include <QThread>
#include <desq/desq-config.h>
#include <desq/Utils.hpp>
#include <DFXdg.hpp>
#include <DFUtils.hpp>
#include <DFSettings.hpp>
#include <DFApplication.hpp>

#include <iostream>

#include "PWBackend.hpp"
#include "TrayIcons.hpp"

DFL::Settings *volSett;

void showHelp() {
    std::cout << "DesQ Volume v" PROJECT_VERSION "\n" << std::endl;
    std::cout << "Usage: desq-volume [options]\n" << std::endl;

    std::cout << "Application Options:" << std::endl;
    std::cout << "  -o|source                      Change the properties of the default source." << std::endl;
    std::cout << "  -k|sink                        Change the properties of the default sink." << std::endl;
    std::cout << "  -s|set-volume <value>          Set the volume of the default source/sink." << std::endl;
    std::cout << "                                 <value> can be absolute or relative percentage." << std::endl;
    std::cout << "  -m|toggle-mute                 Mute/unmute the default source/sink." << std::endl;

    std::cout << "Other Options:" << std::endl;
    std::cout << "  -?|--help                      Print this help and exit." << std::endl;
    std::cout << "  -v|--version                   Print application version and exit." << std::endl;
}


void showVersion() {
    std::cout << "DesQ Volume v" PROJECT_VERSION << std::endl;
    std::cout << "A Pipewire/PulseAudio based volume manager for DesQ Shell.\n" << std::endl;
}


int main( int argc, char *argv[] ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Volume.log" ).toLocal8Bit().data(), "a" );
    qInstallMessageHandler( DFL::Logger );

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Volume started at" << QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8().constData();
    qDebug() << "------------------------------------------------------------------------";

    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );
    DFL::Application *app = new DFL::Application( argc, argv );

    app->setOrganizationName( "DesQ" );
    app->setApplicationName( "Volume" );
    app->setApplicationVersion( PROJECT_VERSION );
    app->setDesktopFileName( "desq-volume" );

    QObject::connect(
        app, &DFL::Application::unixSignalReceived, [ app ] ( int signum ) {
            if ( ( signum == SIGTERM ) || ( signum == SIGINT ) || ( signum == SIGQUIT ) ) {
                app->quit();
            }
        }
    );

    app->setQuitOnLastWindowClosed( false );

    QCommandLineParser *parser = new QCommandLineParser();

    parser->addOption( { { "h", "help" }, "Print this help text" } );
    parser->addOption( { { "v", "version" }, "Print application version and exit." } );
    parser->addOption( { { "k", "sink" }, "Perform operations on sink" } );
    parser->addOption( { { "o", "source" }, "Perform operations on source" } );
    parser->addOption( { { "s", "set-volume" }, "Set the volume of the default sink.", "volume" } );
    parser->addOption( { { "m", "toggle-mute" }, "Mute/Unmute the default sink.", } );

    parser->process( *app );

    if ( parser->isSet( "help" ) ) {
        showHelp();
        return EXIT_SUCCESS;
    }

    else if ( parser->isSet( "version" ) ) {
        showVersion();
        return EXIT_SUCCESS;
    }

    else if ( parser->isSet( "toggle-mute" ) and parser->isSet( "set-volume" ) ) {
        qCritical() << "Please specify one of toggle-mute(m)/volume(v)";
        return EXIT_FAILURE;
    }

    if ( app->lockApplication() ) {
        volSett = DesQ::Utils::initializeDesQSettings( "Volume", "Volume" );

        /** Prepare and show the sink and source tray icons */
        DesQ::Volume::VolumeIcon *volTray = new DesQ::Volume::VolumeIcon();
        DesQ::Volume::MicIcon    *micTray = new DesQ::Volume::MicIcon();

        volTray->show();
        micTray->show();

        /** Start the backend */
        DesQ::Volume::PWBackend *backend = new DesQ::Volume::PWBackend();

        /** Start receiving the CLI events */
        QObject::connect( app,     &DFL::Application::messageFromClient,          backend, &DesQ::Volume::PWBackend::handleMessages );

        /** Start updating the volume icons */
        QObject::connect( volTray, &DesQ::Volume::VolumeIcon::toggleMute,         backend, &DesQ::Volume::PWBackend::toggleSinkMute );
        QObject::connect( backend, &DesQ::Volume::PWBackend::sinkVolumeChanged,   volTray, &DesQ::Volume::VolumeIcon::handleVolumeChange );
        QObject::connect( backend, &DesQ::Volume::PWBackend::sinkMuteChanged,     volTray, &DesQ::Volume::VolumeIcon::handleMuteChange );

        QObject::connect( micTray, &DesQ::Volume::MicIcon::toggleMute,            backend, &DesQ::Volume::PWBackend::toggleSourceMute );
        QObject::connect( backend, &DesQ::Volume::PWBackend::sourceVolumeChanged, micTray, &DesQ::Volume::MicIcon::handleVolumeChange );
        QObject::connect( backend, &DesQ::Volume::PWBackend::sourceMuteChanged,   micTray, &DesQ::Volume::MicIcon::handleMuteChange );

        return app->exec();
    }

    else {
        QString message;

        if ( parser->isSet( "sink" ) == parser->isSet( "source" ) ) {
            qDebug() << "[Error]: Specify one of --sink or --source";
            showHelp();

            return EXIT_FAILURE;
        }

        /** Either sink or source has been set. So, if it's not the sink, it'll be the source */
        message = ( parser->isSet( "sink" ) ? "sink\n" : "source\n" );

        if ( parser->isSet( "toggle-mute" ) ) {
            message += "toggle-mute";
        }

        else if ( parser->isSet( "set-volume" ) ) {
            message += QString( "set-volume" ) + "\n" + parser->value( "set-volume" );
        }

        else {
            std::cout << "DesQ Volume is already running in the background." << std::endl;
            std::cout << "You can interact with the app using CLI" << std::endl << std::endl;
            showHelp();

            return 0;
        }

        app->messageServer( message );
        return 0;
    }

    return 0;
}
