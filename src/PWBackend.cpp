/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "PWBackend.hpp"
#include <DFSettings.hpp>

/* Canberra header */
#include <canberra.h>

DesQ::Volume::PWBackend::PWBackend() : QObject(), pw( nullptr ) {
    mMaxVol = volSett->value( "VolumeCeiling" );

    resetPipewire();

    /** Create the context */
    ca_context *c  = nullptr;
    int        ret = ca_context_create( &c );

    if ( ( ret == CA_SUCCESS ) && c ) {
        chirper.reset( c );

        /** Set the driver to pulse */
        ret = ca_context_set_driver( chirper.get(), "pulse" );
    }

    if ( ret != CA_SUCCESS ) {
        chirper.reset( nullptr );
        qWarning() << "Chirper unavailable: No chirps will be made.";
    }
}


DesQ::Volume::PWBackend::~PWBackend() {
    // Both pointers will be deleted automatically.
}


void DesQ::Volume::PWBackend::handleMessages( const QString& message, int ) {
    QStringList parts = message.split( "\n" );

    if ( ( parts[ 0 ] == "sink" ) && ( parts[ 1 ] == "toggle-mute" ) ) {
        toggleSinkMute();
        return;
    }

    if ( ( parts[ 0 ] == "source" ) && ( parts[ 1 ] == "toggle-mute" ) ) {
        toggleSourceMute();
        return;
    }

    else if ( parts[ 1 ].startsWith( "set-volume" ) ) {
        /** Ensure that the user has used the correct syntax */
        QRegularExpression rx( "[-+]*[0-9]+%" );

        if ( rx.match( parts[ 2 ] ).hasMatch() == false ) {
            return;
        }

        qreal newVol;

        /** +<val>% */
        if ( parts[ 2 ].startsWith( "+" ) ) {
            qreal volPC   = parts[ 2 ].replace( "+", "" ).replace( "%", "" ).toInt() / 100.0;
            qreal curVol  = pw->sinkVolume();
            qreal volPlus = volPC / 100.0;

            newVol = curVol + volPlus;

            if ( newVol > mMaxVol ) {
                newVol = mMaxVol;
            }
        }

        /** -<val>% */
        else if ( parts[ 2 ].startsWith( "-" ) ) {
            qreal volPC    = parts[ 2 ].replace( "-", "" ).replace( "%", "" ).toInt() / 100.0;
            qreal curVol   = pw->sinkVolume();
            qreal volMinus = volPC / 100.0;

            newVol = curVol - volMinus;

            if ( newVol < 0 ) {
                newVol = 0;
            }
        }

        /** <val>% */
        else {
            newVol = 1.0 * parts[ 2 ].replace( "%", "" ).toInt() / 100.0;
        }

        if ( newVol > mMaxVol ) {
            newVol = mMaxVol;
        }

        if ( parts[ 0 ] == "sink" ) {
            pw->setSinkVolume( newVol );
        }

        else {
            pw->setSourceVolume( newVol );
        }
    }
}


void DesQ::Volume::PWBackend::chirp() {
    /** Don't make sounds when the sink is muted or its volume is 0 */
    if ( pw->isSinkMuted() or ( pw->sinkVolume() == 0 ) ) {
        return;
    }

    /** Play the sound */
    ca_context_play(
        chirper.get(),
        0,
        CA_PROP_EVENT_ID,
        "message",
        CA_PROP_EVENT_DESCRIPTION,
        "Chirp",
        NULL
    );
}


void DesQ::Volume::PWBackend::notifyUser( QString target ) {
    QString icon;
    QString title;
    QString hint = "--hint=";

    if ( target == "source" ) {
        qreal newVol = pw->sourceVolume() * 100;

        if ( ( newVol == 0 ) || pw->isSourceMuted() ) {
            icon  = "mic-off";
            title = "Mic: Muted";
        }

        else {
            icon  = "mic-on";
            title = QString( "Mic: %1%" ).arg( newVol );
        }

        hint += QString( "double:percent:%1" ).arg( newVol );
    }

    else {
        int newVol = pw->sinkVolume() * 100;

        if ( ( pw->sinkVolume() == 0 ) || pw->isSinkMuted() ) {
            icon  = "audio-volume-muted";
            title = "Volume: Muted";
        }

        else if ( newVol <= 33 ) {
            icon  = "audio-volume-low";
            title = QString( "Volume: %1%" ).arg( newVol );
        }

        else if ( newVol <= 66 ) {
            icon  = "audio-volume-medium";
            title = QString( "Volume: %1%" ).arg( newVol );
        }

        else if ( newVol <= 100 ) {
            icon  = "audio-volume-high";
            title = QString( "Volume: %1%" ).arg( newVol );
        }

        else {
            icon  = "audio-volume-high-danger";
            title = QString( "Volume: %1%" ).arg( newVol );
        }

        hint += QString( "double:percent:%1" ).arg( newVol );
    }

    QProcess::startDetached(
        "desq-notifier", {
            "-i", icon,
            "-a", "DesQ Volume",
            "-u", "low",
            "-r", ( target == "sink" ? mSinkID : mSourceID ),
            hint,
            title,
            ""
        }
    );
}


void DesQ::Volume::PWBackend::toggleSinkMute() {
    pw->setSinkMuted( !pw->isSinkMuted() );
}


void DesQ::Volume::PWBackend::toggleSourceMute() {
    pw->setSourceMuted( !pw->isSourceMuted() );
}


void DesQ::Volume::PWBackend::resetPipewire() {
    pw.reset( new DFL::Pipewire( this ) );

    connect(
        pw.get(), &DFL::Pipewire::defaultSinkChanged, [ this ] ( uint32_t sinkId ) {
            this->mSinkId = sinkId;
        }
    );

    connect(
        pw.get(), &DFL::Pipewire::sinkVolumeChanged, [ this ] ( qreal volume ) {
            emit this->sinkVolumeChanged( volume * 100 );
            notifyUser( "sink" );
            chirp();
        }
    );

    connect(
        pw.get(), &DFL::Pipewire::sinkMuteChanged, [ this ] ( bool mute ) {
            emit this->sinkMuteChanged( mute );
            notifyUser( "sink" );
            chirp();
        }
    );

    connect(
        pw.get(), &DFL::Pipewire::defaultSourceChanged, [ this ] ( uint32_t sinkId ) {
            this->mSourceId = sinkId;
        }
    );

    connect(
        pw.get(), &DFL::Pipewire::sourceVolumeChanged, [ this ] ( qreal volume ) {
            emit this->sourceVolumeChanged( volume * 100 );
            notifyUser( "source" );
        }
    );

    connect(
        pw.get(), &DFL::Pipewire::sourceMuteChanged, [ this ] ( bool mute ) {
            emit this->sourceMuteChanged( mute );
            notifyUser( "source" );
        }
    );

    connect( pw.get(), &DFL::Pipewire::disconnected, this, &DesQ::Volume::PWBackend::resetPipewire, Qt::QueuedConnection );

    pw->start();
}


void DesQ::Volume::PWBackend::CanberraDeleter::operator()( ca_context *ctx ) const {
    if ( ctx ) {
        ca_context_destroy( ctx );
    }
}
