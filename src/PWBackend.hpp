/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"

/* DFL::Pipewire Headers */
#include <DFPipewire.hpp>

/* Canberra header */
#include <canberra.h>

namespace DesQ {
    namespace Volume {
        class PWBackend;
    }
}

class DesQ::Volume::PWBackend : public QObject {
    Q_OBJECT;

    public:
        PWBackend();
        ~PWBackend();

        /** Handle messages from CLI */
        void handleMessages( const QString&, int );

        /** Chirp when the volume is changed */
        void chirp();

        /** Notify the user the changes in source/sink */
        void notifyUser( QString );

        /** Toggle the sink and source mute */
        Q_SLOT void toggleSinkMute();
        Q_SLOT void toggleSourceMute();

        /** Sink (Output) volume and mute state */
        Q_SIGNAL void sinkVolumeChanged( int newVol );
        Q_SIGNAL void sinkMuteChanged( bool isMuted );

        /** Source (Mic) volume, i.e, boost and mute state */
        Q_SIGNAL void sourceVolumeChanged( int newVol );
        Q_SIGNAL void sourceMuteChanged( bool isMuted );

    private:
        void resetPipewire();

        std::unique_ptr<DFL::Pipewire> pw;

        // pw_main_loop deleter
        struct CanberraDeleter {
            // The operator that actually destroys the pw_main_loop* object;
            void operator()( struct ca_context *pointer ) const;
        };

        std::unique_ptr<struct ca_context, CanberraDeleter> chirper;

        bool isMuted            = false;
        const QString mSinkID   = "141";
        const QString mSourceID = "142";

        qreal mMaxVol = 1.0;

        uint32_t mSinkId   = 0;
        uint32_t mSourceId = 0;
};
