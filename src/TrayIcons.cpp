/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "TrayIcons.hpp"

DesQ::Volume::VolumeIcon::VolumeIcon() : QSystemTrayIcon() {
    setIcon( QIcon::fromTheme( "audio-ready" ) );
    connect( this, &QSystemTrayIcon::activated, this, &DesQ::Volume::VolumeIcon::handleActivation );
}


void DesQ::Volume::VolumeIcon::handleActivation( QSystemTrayIcon::ActivationReason reason ) {
    switch ( reason ) {
        case QSystemTrayIcon::DoubleClick:
        case QSystemTrayIcon::Context: {
            break;
        };

        case QSystemTrayIcon::MiddleClick: {
            break;
        };

        case QSystemTrayIcon::Trigger: {
            emit toggleMute();
            break;
        };

        default: {
            qDebug() << "Something else";
            break;
        };
    }
}


void DesQ::Volume::VolumeIcon::handleVolumeChange( int newVol ) {
    mVolume = newVol;
    setTrayIcon();
}


void DesQ::Volume::VolumeIcon::handleMuteChange( bool isMuted ) {
    mIsMute = isMuted;
    setTrayIcon();
}


void DesQ::Volume::VolumeIcon::setTrayIcon() {
    QString icon;

    if ( ( mVolume == 0 ) || ( mIsMute == true ) ) {
        icon = "audio-volume-muted";
    }

    else if ( mVolume <= 33 ) {
        icon = "audio-volume-low";
    }

    else if ( mVolume <= 66 ) {
        icon = "audio-volume-medium";
    }

    else if ( mVolume <= 100 ) {
        icon = "audio-volume-high";
    }

    else {
        icon = "audio-volume-high-danger";
    }

    setIcon( QIcon::fromTheme( icon ) );

    if ( mIsMute == true ) {
        setToolTip( QString( "Volume: muted" ) );
    }

    else {
        setToolTip( QString( "Volume: %1%" ).arg( mVolume ) );
    }
}


DesQ::Volume::MicIcon::MicIcon() : QSystemTrayIcon() {
    setIcon( QIcon::fromTheme( "mic-ready" ) );
    connect( this, &QSystemTrayIcon::activated, this, &DesQ::Volume::MicIcon::handleActivation );
}


void DesQ::Volume::MicIcon::handleActivation( QSystemTrayIcon::ActivationReason reason ) {
    switch ( reason ) {
        case QSystemTrayIcon::DoubleClick:
        case QSystemTrayIcon::Context: {
            break;
        };

        case QSystemTrayIcon::MiddleClick: {
            break;
        };

        case QSystemTrayIcon::Trigger: {
            emit toggleMute();
            break;
        };

        default: {
            qDebug() << "Something else";
            break;
        };
    }
}


void DesQ::Volume::MicIcon::handleVolumeChange( int newVol ) {
    mVolume = newVol;
    setTrayIcon();
}


void DesQ::Volume::MicIcon::handleMuteChange( bool isMuted ) {
    mIsMute = isMuted;
    setTrayIcon();
}


void DesQ::Volume::MicIcon::setTrayIcon() {
    QString icon;

    if ( ( mVolume == 0 ) || ( mIsMute == true ) ) {
        icon = "mic-off";
    }

    else {
        icon = "mic-on";
    }

    setIcon( QIcon::fromTheme( icon ) );

    if ( mIsMute == true ) {
        setToolTip( QString( "Mic: muted" ) );
    }

    else {
        setToolTip( QString( "Mic: %1%" ).arg( mVolume ) );
    }
}
