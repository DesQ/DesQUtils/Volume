/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"

namespace DesQ {
    namespace Volume {
        class VolumeIcon;
        class MicIcon;
    }
}

class DesQ::Volume::VolumeIcon : public QSystemTrayIcon {
    Q_OBJECT;

    public:
        VolumeIcon();

        Q_SLOT void handleActivation( QSystemTrayIcon::ActivationReason reason );

        Q_SLOT void handleVolumeChange( int newVol );
        Q_SLOT void handleMuteChange( bool isMuted );

        Q_SIGNAL void toggleMute();

    private:
        void setTrayIcon();

        bool mIsMute = false;
        int mVolume  = 50;
};

class DesQ::Volume::MicIcon : public QSystemTrayIcon {
    Q_OBJECT;

    public:
        MicIcon();

        Q_SLOT void handleActivation( QSystemTrayIcon::ActivationReason reason );

        Q_SLOT void handleVolumeChange( int newVol );
        Q_SLOT void handleMuteChange( bool isMuted );

        Q_SIGNAL void toggleMute();

    private:
        void setTrayIcon();

        bool mIsMute = false;
        int mVolume  = 50;
};
